class formations{
    constructor(id, nom, desc, duree, prix){
        this.id = id;
        this.nom = nom;
        this.desc = desc;
        this.duree = duree;
        this.prix = prix;
    }
}

const data = [
    new formations(0, "Mongo et moi", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(1, "Mongo et toi", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(2, "Mongo et nous", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(3, "Mongo et vous", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(4, "Mongo et eux", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(5, "Node et moi", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(6, "Node et toi", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(7, "Node et nous", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(8, "Node et vous", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session"),
    new formations(9, "Node et eux", "Lorem ipsum dolor sit amet, consectetur adipiscing elit. Nunc in tincidunt felis, vel commodo magna. Praesent pellentesque pulvinar massa, ultrices faucibus lacus. Pellentesque nibh eros, elementum ut risus non, porttitor posuere ex.", "16 mois", "50$/session")
];

exports.getFormationById = function(id){
    return data[id];
}

exports.allFormations = data;
