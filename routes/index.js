var express = require('express');
var router = express.Router();
let articles = require('../custom_modules/articles.js');
let formulaires = require('../custom_modules/formulaires.js');
let formations = require('../custom_modules/formations.js');

 router.get('/', function(req, res, next) {
  res.render('index', { title: "Accueil" });
});

router.get('/formations', function(req, res, next){
  res.render('formations', {title: 'Nos formations', formations:formations.allFormations});
})

router.get('/formations/:id', function(req, res, next){
  res.render('formation', {formation: formations.getFormationById(req.params.id)})
})

router.get('/mongodb', function(req, res, next){
  res.render('mongodb', { title: 'Formations MongoDB', formations:formations.allFormations});
})

router.get('/nodejs', function(req, res, next){
  res.render('nodejs', { title: 'Formations Nodejs', formations:formations.allFormations});
})

router.get('/blog', function(req, res, next){
  res.render('blog', { title: 'Le blog', articles: articles.allArticles});
})

router.get('/blog/:id', function(req, res, next){
  res.render('article', {article: articles.getArticleById(req.params.id)})
})

router.get('/contact', function(req, res, next){
  res.render('contact', { title: 'Nous contacter'});
})

router.post('/contact', function(req, res, next){
  let temp = formulaires.addFormulaire(req.body);
  if(temp){
    res.render('confirmation');
  }
})


module.exports = router;
